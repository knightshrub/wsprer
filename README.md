# WSPRer - Low power WSPR beacon

This project will eventually be a WSPR beacon for the shortwave amateur radio bands.

The WSPR protocol was developed by [Joe Taylor (K1JT)](http://physics.princeton.edu/pulsar/K1JT/wspr.html)
and is used to probe possible propagation paths for amateur radio using low power
transmitters (beacons).
