#ifndef SRCENC_H
#define SRCENC_H

#include <stdint.h>

typedef struct{
    enum{
      MSGTYPE1,		// e.g. K1ABC FN42 37
      MSGTYPE2,		// e.g. PJ4/K1ABC 37
      MSGTYPE3		// e.g. <PJ4/K1ABC> FK52UD 37
  	} mtype;

	char prefix[4];	// callsign prefix
	char call[7];	// callsign
	char loc[7];	// locator
	char dbm[4];	// power level in dBm

	uint32_t pcall;	// packed callsign
	uint32_t ploc;	// packed locator
	uint32_t pdbm;	// packed power level

	double dlat;
	double dlong;

	uint8_t srcenc[7]; // source encoded WSPR message

} tWsprMsg;

void parseWsprMsg(char* s, tWsprMsg* msg);
void srcEncMsg(tWsprMsg* msg);

void packCall(tWsprMsg* msg);
void packLoc(tWsprMsg* msg);
void packPower(tWsprMsg* msg);
void pack50(uint32_t n1, uint32_t n2, tWsprMsg* msg);

void grid2deg(tWsprMsg* msg);
uint32_t nchar(char c);

#endif /* SRCENC_H */
