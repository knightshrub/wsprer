#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include "srcenc.h"

int main(void)
{
	tWsprMsg wmsg;

	parseWsprMsg("K1ABC FN42 37", &wmsg);
	//parseWsprMsg("PJ4/K1ABC 37", &wmsg);
	//parseWsprMsg("<PJ4/K1ABC> FK52UD 37", &wmsg);

	srcEncMsg(&wmsg);

	printf("mtype: %i \n",wmsg.mtype);
	printf("pre: %s \n", wmsg.prefix);
	printf("call: %s \n", wmsg.call);
	printf("loc: %s \n", wmsg.loc);
	printf("dbm: %s \n", wmsg.dbm);

	printf("%#x\n", wmsg.pcall);
	printf("%#x\n", wmsg.ploc);
	printf("%i\n\n", wmsg.pdbm);

	for(uint8_t i = 0; i < 7; i++) printf("%#x\n", wmsg.srcenc[i]);


	return 0;
}
