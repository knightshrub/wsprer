#include <stdio.h>
#include <stdlib.h>

#include "srcenc.h"

/* Parse an input string for WSPR into internal data structure */
void parseWsprMsg(char* s, tWsprMsg* msg)
{
	uint8_t i = 0;
	uint8_t j = 0;

	if(s[0] == '<') msg->mtype = MSGTYPE3;
	else if (s[3] == '/') msg->mtype = MSGTYPE2;
	else msg->mtype = MSGTYPE1;

	switch(msg->mtype)
	{
		case MSGTYPE1:
			while(s[i] != ' ') msg->call[j++] = s[i++];	// copy until delim is reached (space)
			while(j < 6) msg->call[j++] = ' ';			// pad with spaces to 6 characters
			msg->call[j] = 0;							// terminate string

			j = 0;
			i++;

			while(s[i] != ' ') msg->loc[j++] = s[i++];	// copy until delim is reached (space)
			while(j < 6) msg->loc[j++] = 'm';			// pad with m to 6 characters
			msg->loc[j] = 0;							// terminate string

			j = 0;
			i++;

			while(s[i]) msg->dbm[j++] = s[i++];
			msg->dbm[j] = 0;

			break;

		case MSGTYPE2:
			while(s[i] != '/') msg->prefix[j++] = s[i++];// copy until delim reached
			msg->prefix[j] = 0;							// terminate string

			i++;
			j = 0;

			while(s[i] != ' ') msg->call[j++] = s[i++];	// copy until delim is reached (space)
			while(j < 6) msg->call[j++] = ' ';			// pad with spaces to 6 characters
			msg->call[j] = 0;							// terminate string

			j = 0;
			i++;

			while(s[i]) msg->dbm[j++] = s[i++];
			msg->dbm[j] = 0;

			break;

		case MSGTYPE3:
			i++;

			while(s[i] != '/') msg->prefix[j++] = s[i++]; // copy until delim reached
			msg->prefix[j] = 0;							// terminate string

			j = 0;
			i++;

			while(s[i] != '>') msg->call[j++] = s[i++];	// copy until delim is reached (space)
			while(j < 6) msg->call[j++] = ' ';			// pad with spaces to 6 characters
			msg->call[j] = 0;							// terminate string

			j = 0;
			i += 2;

			while(s[i] != ' ') msg->loc[j++] = s[i++];	// copy until delim is reached (space)
			while(j < 6) msg->loc[j++] = 'm';			// pad with m to 6 characters
			msg->loc[j] = 0;							// terminate string

			j = 0;
			i++;

			while(s[i]) msg->dbm[j++] = s[i++];
			msg->dbm[j] = 0;

			break;
	}

	// format callsign correctly
	if( (msg->call[2] >= '0') && (msg->call[2] <= '9') );
	else if( (msg->call[1] >= '0') && (msg->call[1] <= '9') )
	{
		msg->call[5] = msg->call[4];
		msg->call[4] = msg->call[3];
		msg->call[3] = msg->call[2];
		msg->call[2] = msg->call[1];
		msg->call[1] = msg->call[0];
		msg->call[0] = ' ';
	}

	// format locator so that digits 5 and 6 are lowercase FK52UD --> FK52ud
	if( (msg->loc[4] >= 'A') && (msg->loc[4] <= 'Z') ) msg->loc[4] = msg->loc[4] - 'A' + 'a';
	if( (msg->loc[5] >= 'A') && (msg->loc[5] <= 'Z') ) msg->loc[5] = msg->loc[5] - 'A' + 'a';
}


/* Converts Maidenhead grid locator to degrees of West longitude and North latitude */
void grid2deg(tWsprMsg* msg)
{
	double xminlong, xminlat;
	int32_t nlong, n20d;
	int32_t nlat;

	nlong = 180 - 20*(msg->loc[0]-'A');
	n20d = 2*(msg->loc[2]-'0');
	xminlong = 5*(0.5 + (double)(msg->loc[4] - 'a'));
	msg->dlong = nlong - n20d - xminlong/60.0;

	nlat = -90 + 10*(msg->loc[1] - 'A') + msg->loc[3] - '0';
	xminlat = 2.5*(0.5 + (double)(msg->loc[5] - 'a'));
	msg->dlat = nlat + xminlat/60.0;
}

/* Maps characters (0-9,A-Z,a-z,space) into range [0,36] */
uint32_t nchar(char c)
{
	uint32_t n;

	if( (c >= '0') && (c <= '9') ) n = c - '0';
	else if( (c >= 'A') && (c <= 'Z') ) n = c - 'A' + 10;
	else if( (c >= 'a') && (c <= 'z') ) n = c - 'a' + 10;
	else if(c == ' ') n = 36;

	return n;
}

/* Pack callsign into 28 bits according to K1JT implementation */
void packCall(tWsprMsg* msg)
{
	uint32_t ncall;

	ncall = nchar(msg->call[0]);
	ncall = 36*ncall + nchar(msg->call[1]);
	ncall = 10*ncall + nchar(msg->call[2]);
	ncall = 27*ncall + nchar(msg->call[3]) - 10;
	ncall = 27*ncall + nchar(msg->call[4]) - 10;
	ncall = 27*ncall + nchar(msg->call[5]) - 10;

	msg->pcall = ncall;
}

/* Pack locator into 15 bit bits according to K1JT implementation */
void packLoc(tWsprMsg* msg)
{
	uint32_t nloc;

	grid2deg(msg);
	nloc = (((uint32_t)msg->dlong + 180)>>1)*180 + (uint32_t)(msg->dlat + 90.0);

	msg->ploc = nloc;
}

/* Pack power level in dBm into 7 bits according to K1JT implementation */
void packPower(tWsprMsg* msg)
{
	char conv[10] = {0,-1,1,0,-1,2,1,0,-1,1};
	char power;

	power = (char)atoi(msg->dbm);
	if (power < 0) power = 0;
	else if (power > 60) power = 60;
	power = power + conv[power%10];
	msg->pdbm = (uint32_t)power;
}

/* Packs n1 and n2 into 50 bit WSPR message */
void pack50(uint32_t n1, uint32_t n2, tWsprMsg* msg)
{
	msg->srcenc[0] = (n1>>20) & 0xFF;
	msg->srcenc[1] = (n1>>12) & 0xFF;
	msg->srcenc[2] = (n1>>4) & 0xFF;
	msg->srcenc[3] = ((n1 & 0x0F)<<4) | ((n2>>18) & 0x0F);
	msg->srcenc[4] = (n2>>10) & 0xFF;
	msg->srcenc[5] = (n2>>2) & 0xFF;
	msg->srcenc[6] = ((n2 & 0x03)<<6);
}

/* Source encode a WSPR message */
void srcEncMsg(tWsprMsg* msg)
{
	uint32_t temp;

	packCall(msg);
	packLoc(msg);
	packPower(msg);

	switch(msg->mtype)
	{
		case MSGTYPE1:
			temp = (msg->ploc<<7) + (msg->pdbm + 64);
			pack50(msg->pcall,temp,msg);
			break;

		case MSGTYPE2:
			break;

		case MSGTYPE3:
			break;
	}
}
